package com.pim4j.common;

import java.io.File;

import org.apache.commons.lang3.StringUtils;

public class FileHelper {

	public static boolean isImageFile(File file) {
		final String name = file.getName();
		return StringUtils.endsWithIgnoreCase(name, "gif") || StringUtils.endsWithIgnoreCase(name, "jpg") || StringUtils.endsWithIgnoreCase(name, "png") || StringUtils.endsWithIgnoreCase(name, "bmp");
	}

	public static boolean isTextFile(File file) {
		return file.getName().endsWith(".txt") || file.getName().endsWith(".xml") || file.getName().endsWith(".sql");
	}
}
