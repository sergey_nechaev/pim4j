package com.pim4j.common;

import javax.swing.JScrollPane;

public class SwingHelper {

	public static void updateScrollPane(JScrollPane scroll) {
		scroll.setWheelScrollingEnabled(true);
		scroll.getVerticalScrollBar().setUnitIncrement(16);
		scroll.getHorizontalScrollBar().setUnitIncrement(16);
		
	}
	
}
