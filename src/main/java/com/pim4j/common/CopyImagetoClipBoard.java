package com.pim4j.common;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.awt.image.BufferedImage;

// http://stackoverflow.com/questions/4552045/copy-bufferedimage-to-clipboard
public class CopyImagetoClipBoard implements ClipboardOwner {
	
	public void CopyImage(BufferedImage bi) {
		try {
			TransferableImage trans = new TransferableImage(bi);
			Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
			c.setContents(trans, this);
		} catch (Exception x) {
			x.printStackTrace();
			System.exit(1);
		}
	}

	@Override
	public void lostOwnership(Clipboard clipboard, Transferable contents) {
		
	}
}