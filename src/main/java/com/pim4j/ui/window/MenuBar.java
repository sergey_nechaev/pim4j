package com.pim4j.ui.window;

import java.awt.Desktop;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pim4j.domain.Item;
import com.pim4j.service.ConfigService;
import com.pim4j.service.IconService;
import com.pim4j.ui.tree.Tree;

@SuppressWarnings("serial")
@Component
public class MenuBar extends JMenuBar implements ActionListener {

	private Log log = LogFactory.getLog(this.getClass());

	private static final String NEW_FOLDER = "NEW_FOLDER";
	private static final String NEW_TEXT_NOTE = "NEW_TEXT_NOTE";
	private static final String NEW_FILE = "NEW_FILE";

	private static final String OPEN_DATA_FOLDER = "OPEN_DATA_FOLDER";
	private static final String OPEN_LOG_FOLDER = "OPEN_LOG_FOLDER";

	@Autowired
	private IconService icons;

	@Autowired
	private ConfigService config;

	@Autowired
	private Tree tree;

	@PostConstruct
	public void setupMenubar() {

		JMenu file = new JMenu("File");
		file.setMnemonic('F');
		add(file);

		// file
		{
			JMenuItem newFolder = new JMenuItem("New folder", icons.getFolderAdd());
			newFolder.setActionCommand(NEW_FOLDER);
			newFolder.setMnemonic(KeyEvent.VK_F);
			newFolder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.CTRL_MASK));
			newFolder.addActionListener(this);
			file.add(newFolder);
		}
		{
			JMenuItem newNote = new JMenuItem("New text note", icons.getTextNoteAdd());
			newNote.setActionCommand(NEW_TEXT_NOTE);
			newNote.setMnemonic(KeyEvent.VK_N);
			newNote.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK));
			newNote.addActionListener(this);
			file.add(newNote);
		}

		JMenu help = new JMenu("Help");
		help.setMnemonic('H');
		add(help);

		help.add(new JMenuItem("Help", icons.getHelpIcon()));
		help.addSeparator();

		JMenuItem openDataFolder = new JMenuItem("Open data folder...", icons.getDatabaseIcon());
		openDataFolder.setActionCommand(OPEN_DATA_FOLDER);
		openDataFolder.addActionListener(this);
		help.add(openDataFolder);

		JMenuItem openlogFolder = new JMenuItem("View application logs", icons.getScriptIcon());
		openlogFolder.setActionCommand(OPEN_LOG_FOLDER);
		openlogFolder.addActionListener(this);
		help.add(openlogFolder);

		help.addSeparator();

		JMenuItem about = new JMenuItem("About", icons.getAppIcon());
		about.addActionListener(this);
		help.add(about);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (OPEN_DATA_FOLDER.equals(e.getActionCommand())) {
			try {
				if (Desktop.isDesktopSupported()) {
					Desktop.getDesktop().open(new File(config.getRootFolder()));
				}
			} catch (IOException ex) {
				log.error("Error: ", ex);
			}
		} else if (OPEN_LOG_FOLDER.equals(e.getActionCommand())) {
			try {
				if (Desktop.isDesktopSupported()) {
					File loc = new File(config.getLogFolder());
					log.debug("Open: " + loc);
					Desktop.getDesktop().open(loc);
				}
			} catch (IOException ex) {
				log.error("Error: ", ex);
			}

		} else if (NEW_FOLDER.equals(e.getActionCommand())) {

			List<Item> folders = tree.addFolder(tree.getSelectedItem());
			tree.addItemsToTree(tree.getSelectedNode(), folders);

		} else if (NEW_TEXT_NOTE.equals(e.getActionCommand())) {
			List<Item> items = tree.addTextNote(tree.getSelectedItem());
			tree.addItemsToTree(tree.getSelectedNode(), items);

		}
	}

}
