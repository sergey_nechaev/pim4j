package com.pim4j.ui.window;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.annotation.PostConstruct;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.jidesoft.swing.JideSplitPane;
import com.pim4j.Pim4j;
import com.pim4j.service.IconService;
import com.pim4j.service.SearchResults;
import com.pim4j.service.SearchService;
import com.pim4j.ui.tabs.Tabs;
import com.pim4j.ui.tree.Tree;

import javafx.scene.layout.Border;
import net.oss.swing.java.text.context.menu.QTextContextMenu;

@Component
public class MainFrame extends JFrame {

	@Value("${app.version}")
	private String appVersion;

	@Autowired
	private Tree itemsExplorer;

	@Autowired
	private SearchService searchService;

	@Autowired
	private Tabs tabs;

	@Autowired
	private IconService icons;

	@Autowired
	private MenuBar menubar;

	private JideSplitPane split;

	private JTextField textSearch;

	@PostConstruct
	public void init() {

		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setSize(800, 600);
		this.setIconImage(icons.getAppIcon().getImage());
		this.setTitle("Pim4j " + appVersion);
		this.setLocationRelativeTo(null);
		this.setMinimumSize(new Dimension(300, 300));

		this.setJMenuBar(menubar);

		split = new JideSplitPane();
		split.setDividerSize(8);
		split.add(new JScrollPane(itemsExplorer));

		JPanel p = new JPanel(new BorderLayout());
		p.add(tabs, BorderLayout.CENTER);

		// search panel
		JPanel sp = new JPanel(new BorderLayout());
		sp.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		JPanel spHolder = new JPanel(new BorderLayout());
		spHolder.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
		spHolder.add(sp);

		p.add(spHolder, BorderLayout.NORTH);

		textSearch = new JTextField();
		new QTextContextMenu(textSearch);
		textSearch.addActionListener((e) -> {
			search(textSearch.getText());
		});
		sp.add(textSearch);

		JButton btnSearch = new JButton("Search...", icons.getMagnifier());
		btnSearch.addActionListener((e) -> {
			search(textSearch.getText());
		});

		btnSearch.setMnemonic('S');

		sp.add(btnSearch, BorderLayout.EAST);

		split.add(p);

		this.setLayout(new BorderLayout());
		this.add(split, BorderLayout.CENTER);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				Pim4j.exit();
			}
		});

	}

	public void search(String query) {

		SearchResults result = searchService.search(query);

		tabs.addSearchResults(result);

		textSearch.selectAll();

	}

	public void run() {
		this.setVisible(true);
		split.setDividerLocation(0, 200);
		textSearch.requestFocus();

	}

}
