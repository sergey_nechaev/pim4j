package com.pim4j.ui.tabs.search;

import java.awt.BorderLayout;
import java.awt.Font;

import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;

import org.apache.commons.lang3.StringUtils;

import com.pim4j.Pim4j;
import com.pim4j.service.SearchResults;
import com.pim4j.service.SearchResults.SearchResult;
import com.pim4j.ui.tabs.FocusableTab;

import net.oss.swing.java.text.context.menu.QTextContextMenu;

@SuppressWarnings("serial")
public class TabSearchResults extends JPanel implements HyperlinkListener,FocusableTab {

	private JEditorPane editor = new JEditorPane();

	public TabSearchResults(SearchResults result) {

		this.setLayout(new BorderLayout());
		this.add(new JScrollPane(editor));

		editor.setContentType("text/html");
		editor.setEditable(false);
		new QTextContextMenu(editor);

		StringBuilder sb = new StringBuilder();
		
		sb.append("<div style='background:yellow;padding:3px;'>Searching for <b>" + result.getQuery() + "</b>: total results: " + result.getTotal() + "</div>");

		for (SearchResult sr : result.getResults()) {
			sb.append("<h3><a href='#" + sr.uuid + "'>" + sr.title + "</a></h3>");
			sb.append("<div>" + sr.snippet + "</div>");
			sb.append("<br/>");
		}

	    Font font = editor.getFont();
	    String bodyRule = "body { font-family: " + font.getFamily() + "; " +
	            "font-size: " + font.getSize() + "pt; }";
	    
	    ((HTMLDocument)editor.getDocument()).getStyleSheet().addRule(bodyRule);
	    
		editor.setText("<html><body>" + sb.toString() + "</body></html>");

		editor.setText(sb.toString());
		
		editor.addHyperlinkListener(this);

	}

	@Override
	public void hyperlinkUpdate(HyperlinkEvent e) {
		if (e.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED)) {
			String uuid = StringUtils.removeStart(e.getDescription(), "#");
			System.out.println("Clicked on " + uuid);
			Pim4j.getTree().selectAndOpenByUuid(uuid);
		}
	}

	@Override
	public void focus() {
		editor.requestFocusInWindow();
	}
}
