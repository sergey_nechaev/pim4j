package com.pim4j.ui.tabs.contact;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.pim4j.common.SwingHelper;
import com.pim4j.domain.Item;
import com.pim4j.ui.tabs.EditableTab;
import com.pim4j.ui.tabs.FocusableTab;

public class TabContact extends JPanel implements EditableTab, FocusableTab {
	
	private Item item;
	
	private ContactForm form;
	
	public TabContact(Item item) {
		
		super(new BorderLayout());
		
		this.item = item;
		form = new ContactForm();
		
		JScrollPane scroll = new JScrollPane(form);
		SwingHelper.updateScrollPane(scroll);
		
		this.add(scroll,BorderLayout.CENTER);
		
	}

	@Override
	public String getUuid() {
		return item.getUuid();
	}

	@Override
	public String getTitle() {
		return item.getName();
	}

	@Override
	public void focus() {
		
	}

	@Override
	public boolean isChanged() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean setChanged(boolean f) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getText() {
		// TODO Auto-generated method stub
		return null;
	}

}
