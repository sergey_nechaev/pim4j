package com.pim4j.ui.tabs;

public interface EditableTab extends Tab {

	boolean isChanged();
	
	boolean setChanged(boolean f);

	String getText();

	
}