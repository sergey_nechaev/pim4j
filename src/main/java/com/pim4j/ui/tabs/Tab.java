package com.pim4j.ui.tabs;

public interface Tab {

	String getUuid();

	String getTitle();

}
