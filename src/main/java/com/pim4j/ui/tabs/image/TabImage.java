package com.pim4j.ui.tabs.image;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.pim4j.Pim4j;
import com.pim4j.common.CopyImagetoClipBoard;
import com.pim4j.common.SwingHelper;
import com.pim4j.service.IconService;
import com.pim4j.ui.tabs.FocusableTab;
import com.pim4j.ui.tabs.Tab;

@SuppressWarnings("serial")
public class TabImage extends JPanel implements Tab, FocusableTab, ActionListener {

	private Log log = LogFactory.getLog(this.getClass());

	private String name;
	private String uuid;
	private BufferedImage bim;
	private JPanel panel;

	private static final String ACTION_COPY_IMAGE_TO_CLIPBOARD = "ACTION_COPY_IMAGE_TO_CLIPBOARD";
	private static final String ACTION_SAVE_TO_FILE = "ACTION_SAVE_TO_FILE";

	class ContextMenu extends JPopupMenu {
		public ContextMenu() {
			JMenuItem clipboard = new JMenuItem("Copy image to clipboard", IconService.load("pictures"));
			clipboard.addActionListener(TabImage.this);
			clipboard.setActionCommand(ACTION_COPY_IMAGE_TO_CLIPBOARD);
			add(clipboard);
			/*
			 * addSeparator(); JMenuItem save = new JMenuItem(
			 * "Save image as a file...", IconService.load("picture_save"));
			 * save.addActionListener(TabImage.this);
			 * save.setActionCommand(ACTION_SAVE_TO_FILE); add(save);
			 */
		}
	}

	public TabImage(String name, String uuid, BufferedImage data) {

		super(new BorderLayout());

		this.name = name;
		this.uuid = uuid;
		this.bim = data;

		panel = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				
				int x = (this.getWidth() - bim.getWidth()) / 2;
				int y = (this.getHeight() - bim.getHeight()) / 2;

				g.drawImage(bim, x >= 0 ? x : 0, y >= 0 ? y : 0, null);
			}
		};
		panel.addMouseListener(new MouseAdapter() {

			@Override
			public void mouseClicked(MouseEvent e) {
				new ContextMenu().show(e.getComponent(), e.getX(), e.getY());
			}

		});

		final Dimension size = new Dimension(bim.getWidth(), bim.getHeight());

		panel.setMinimumSize(size);
		panel.setMaximumSize(size);
		panel.setPreferredSize(size);
		panel.setToolTipText(name + "(" + bim.getWidth() + "x" + bim.getHeight() + ")");

		JScrollPane scroll = new JScrollPane(panel);
		SwingHelper.updateScrollPane(scroll);

		this.add(scroll, BorderLayout.CENTER);

	}

	@Override
	public String getUuid() {
		return uuid;
	}

	@Override
	public String getTitle() {
		return name;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		log.debug(e.getActionCommand());
		if (ACTION_COPY_IMAGE_TO_CLIPBOARD.equals(e.getActionCommand())) {
			log.debug("Copy image to clipboard");
			CopyImagetoClipBoard ci = new CopyImagetoClipBoard();
			ci.CopyImage(bim);
			Pim4j.getSystemTray().showInfoMessage("Success", "The image was copied to clipboard");
		} else if (ACTION_SAVE_TO_FILE.equals(e.getActionCommand())) {

			saveImageToFile();

		}
	}

	private void saveImageToFile() {

		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Save image to file");
		fc.setPreferredSize(new Dimension(800, 600));

		int returnVal = fc.showSaveDialog(this);

	}

	@Override
	public void focus() {
		panel.requestFocusInWindow();
	}

}
