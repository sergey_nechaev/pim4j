package com.pim4j.ui.tabs.text;

import java.awt.BorderLayout;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

import com.jidesoft.swing.TextComponentSearchable;
import com.pim4j.ui.tabs.EditableTab;
import com.pim4j.ui.tabs.FocusableTab;

import net.oss.swing.java.text.context.menu.QTextContextMenu;

@SuppressWarnings("serial")
public class TabTextNote extends JPanel implements DocumentListener, EditableTab, FocusableTab {

	private JTextArea editor = new JTextArea();

	private boolean changed = true;

	private String title;

	private String uuid;

	public TabTextNote(String title, String uuid, String text) {

		this.title = title;
		this.uuid = uuid;

		new QTextContextMenu(editor);
		this.setLayout(new BorderLayout());
		JScrollPane scroll = new JScrollPane(editor);
		this.add(scroll);

		scroll.addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (e.isControlDown()) {
					float size = (float) editor.getFont().getSize();
					size -= e.getWheelRotation();
					if (size < 10) {
						size = 10;
					} else if (size > 30) {
						size = 30;
					}
					editor.setFont(editor.getFont().deriveFont(size));
				}
			}
		});

		editor.setText(text);
		TextComponentSearchable searchable = new TextComponentSearchable(editor);

		Document doc = editor.getDocument();
		if (doc instanceof PlainDocument) {
			doc.putProperty(PlainDocument.tabSizeAttribute, 4);
		}

		editor.getDocument().addDocumentListener(this);

		editor.requestFocus();
	}

	@Override
	public void insertUpdate(DocumentEvent e) {
		changed = true;
	}

	@Override
	public void removeUpdate(DocumentEvent e) {
		changed = true;
	}

	@Override
	public void changedUpdate(DocumentEvent e) {
		changed = true;
	}

	@Override
	public boolean isChanged() {
		return changed;
	}

	@Override
	public String getUuid() {
		return uuid;
	}

	@Override
	public String getText() {
		return editor.getText();
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public boolean setChanged(boolean f) {
		return changed = f;
	}

	@Override
	public void focus() {
		editor.requestFocusInWindow();
		editor.setCaretPosition(0);
	}

}
