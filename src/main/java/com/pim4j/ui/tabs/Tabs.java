package com.pim4j.ui.tabs;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jidesoft.swing.JideTabbedPane;
import com.pim4j.domain.Item;
import com.pim4j.service.IconService;
import com.pim4j.service.ItemService;
import com.pim4j.service.SearchResults;
import com.pim4j.service.SearchService;
import com.pim4j.ui.tabs.contact.TabContact;
import com.pim4j.ui.tabs.image.TabImage;
import com.pim4j.ui.tabs.search.TabSearchResults;
import com.pim4j.ui.tabs.text.TabTextNote;

@Component
public class Tabs extends JPanel implements ChangeListener {

	private final Log log = LogFactory.getLog(this.getClass());

	private JideTabbedPane tabs;

	@Autowired
	private SearchService searchService;

	@Autowired
	private ItemService itemService;

	@Autowired
	private IconService icons;

	@PostConstruct
	public void init() {

		tabs = new JideTabbedPane();
		tabs.addChangeListener(this);
		tabs.setUseDefaultShowCloseButtonOnTab(false);
		tabs.setShowCloseButtonOnTab(true);
		tabs.setBoldActiveTab(true);
		tabs.setColorTheme(JideTabbedPane.COLOR_THEME_VSNET);
		tabs.setTabShape(JideTabbedPane.SHAPE_OFFICE2003);
		tabs.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				log.debug("Tabs status changed: " + e);
			}
		});

		this.setLayout(new BorderLayout());
		this.add(tabs, BorderLayout.CENTER);

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {

					for (int i = 0; i < tabs.getTabCount(); i++) {
						java.awt.Component tabComponentAt = tabs.getComponentAt(i);
						if (tabComponentAt instanceof EditableTab) {
							EditableTab tab = (EditableTab) tabComponentAt;

							if (tab.isChanged()) {

								log.debug(tab.getTitle() + " - " + tab.isChanged());
								// add to search index

								String newContent = tab.getText();
								if (newContent != null) {
									searchService.index(tab.getUuid(), tab.getTitle(), newContent);
									tab.setChanged(false);
									// save content
									itemService.saveContent(tab.getUuid(), newContent);
								}

							}
						}

					}

					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
				}
			}
		}).start();

	}
	
	@PreDestroy
	public void dispose() {
		log.debug("Disposing tabs");
		for (int i = 0; i < tabs.getTabCount(); i++) {
			java.awt.Component tabComponentAt = tabs.getComponentAt(i);
			if (tabComponentAt instanceof DisposableTab) {
				DisposableTab tab = (DisposableTab) tabComponentAt;
				tab.dispose();
			}
		}		
	}

	private void addTab(String title, Icon icon, Tab tabPanel) {

		if (false == openTabByUuid(tabPanel.getUuid())) {
			tabs.addTab(title, icon, (JComponent) tabPanel);
			tabs.setSelectedIndex(tabs.getTabCount() - 1);
		}

	}

	public boolean openTabByUuid(String uuid) {

		for (int i = 0; i < tabs.getTabCount(); i++) {
			java.awt.Component tabComponentAt = tabs.getComponentAt(i);
			if (tabComponentAt instanceof Tab) {
				Tab tab = (Tab) tabComponentAt;
				if (tab.getUuid().equals(uuid)) {
					tabs.setSelectedIndex(i);
					return true;
				}
			}
		}

		return false;

	}

	public void addTextNote(String title, String uuid, String text) {
		addTab(title, icons.getTextNoteIcon(), new TabTextNote(title, uuid, text));
	}

	public void addSearchResults(SearchResults result) {

		if (result.getTotal() == 0) {
			JOptionPane.showMessageDialog(null, "Nothing found...", "Try Again", JOptionPane.INFORMATION_MESSAGE);
			return;
		}

		String title = result.getQuery();

		if (title.length() > 32) {
			title = StringUtils.substring(title, 0, 32) + "...";
		}

		tabs.addTab("" + title + " - search", icons.getMagnifier(), new TabSearchResults(result));
		tabs.setSelectedIndex(tabs.getTabCount() - 1);

	}

	public void closeTabByUuid(String uuid) {

		for (int i = 0; i < tabs.getTabCount(); i++) {
			java.awt.Component tabComponentAt = tabs.getComponentAt(i);
			if (tabComponentAt instanceof EditableTab) {
				EditableTab tab = (EditableTab) tabComponentAt;
				if (tab.getUuid().equals(uuid)) {
					tabs.remove(i);
					return;
				}
			}
		}

	}

	@Override
	public void stateChanged(ChangeEvent e) {
		if (tabs.getSelectedComponent() != null && tabs.getSelectedComponent() instanceof FocusableTab) {
			FocusableTab f = (FocusableTab) tabs.getSelectedComponent();
			f.focus();
		}
		System.out.println(e.getSource());
	}

	public void addImage(String name, String uuid, BufferedImage data) {
		addTab(name, icons.getImageIcon(), new TabImage(name, uuid, data));

	}

	public void addContact(Item item) {
		addTab(item.getName(), icons.getUserIcon(), new TabContact(item));
	}

}
