package com.pim4j.ui.tabs;

public interface FocusableTab {

	void focus();
}
