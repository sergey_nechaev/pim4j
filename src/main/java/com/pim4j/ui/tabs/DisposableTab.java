package com.pim4j.ui.tabs;

public interface DisposableTab {

	void dispose();
}
