package com.pim4j.ui.tree;

import java.util.EventObject;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;

public class TreeCellEditor extends DefaultTreeCellEditor {

	public TreeCellEditor(JTree tree, DefaultTreeCellRenderer renderer) {
		super(tree, renderer);
	}

	@Override
	public boolean isCellEditable(EventObject e) {
		return super.isCellEditable(e) && ((TreeNode) lastPath.getLastPathComponent()).getParent()!=null;
	}

	@Override
	public java.awt.Component getTreeCellEditorComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row) {
		java.awt.Component c = super.getTreeCellEditorComponent(tree, value, isSelected, expanded, leaf, row);
		return c;
	}
	
	
}
