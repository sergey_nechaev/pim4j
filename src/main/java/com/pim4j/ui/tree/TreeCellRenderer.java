package com.pim4j.ui.tree;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.springframework.beans.factory.annotation.Autowired;

import com.pim4j.domain.FileItem;
import com.pim4j.domain.Item;
import com.pim4j.domain.Item.Type;
import com.pim4j.domain.RootFolder;
import com.pim4j.service.IconService;

@SuppressWarnings("serial")
@org.springframework.stereotype.Component
public class TreeCellRenderer extends DefaultTreeCellRenderer {

	private static final FileSystemView view = FileSystemView.getFileSystemView();

	@Autowired
	private IconService icons;

	private TreeCellRenderer() {
	}

	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {

		JLabel label = (JLabel) super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;

		if (node.getUserObject().getClass().equals(RootFolder.class)) {
			label.setIcon(icons.getRootIcon());
		} else {
			
			if (node.getUserObject() instanceof Item) {

				Item item = (Item) node.getUserObject();

				if (item.getType() == Type.FOLDER) {
					label.setIcon(icons.getFolderIcon());
				} else if (item.getType() == Type.TEXT_NOTE) {
					label.setIcon(icons.getTextNoteIcon());
				} else if (item.getType() == Type.FILE) {
					FileItem fi = (FileItem) item;
					label.setIcon(icons.getSystemIconByContentType(fi.getContentType()));
				} else if (item.getType() == Type.IMAGE) {
					label.setIcon(icons.getImageIcon());
				} else if (item.getType() == Type.CONTACT) {
					label.setIcon(icons.getUserIcon());
				} else if (item.getType() == Type.URL) {
					label.setIcon(icons.getLinkIcon());
				}
			}

		}

		return label;
	}

	@Override
	public Icon getDefaultOpenIcon() {
		return icons.getBulletGoIcon();
	}

	@Override
	public Icon getDefaultClosedIcon() {
		return icons.getBulletGoIcon();
	}

	@Override
	public Icon getDefaultLeafIcon() {
		return icons.getBulletGoIcon();
	}

	@Override
	public Icon getOpenIcon() {
		return icons.getBulletGoIcon();
	}

	@Override
	public Icon getClosedIcon() {
		return icons.getBulletGoIcon();
	}

	@Override
	public Icon getLeafIcon() {
		return icons.getBulletGoIcon();
	}
	
	

}
