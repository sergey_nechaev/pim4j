package com.pim4j.ui.tree;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.TransferHandler;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pim4j.common.FileHelper;
import com.pim4j.domain.Item;

@SuppressWarnings("serial")
@Component
public class TreeTransferHandler extends TransferHandler {

	private Log log = LogFactory.getLog(this.getClass());

	@Autowired
	private Tree tree;

	private Set<DataFlavor> flavours = new HashSet<>();

	@PostConstruct
	public void init() throws ClassNotFoundException {

		flavours.add(DataFlavor.javaFileListFlavor);
		flavours.add(DataFlavor.stringFlavor);
		flavours.add(DataFlavor.imageFlavor);
		flavours.add(DataFlavor.imageFlavor);

		String mimeType = DataFlavor.javaJVMLocalObjectMimeType + ";class=\"" + javax.swing.tree.DefaultMutableTreeNode[].class.getName() + "\"";
		flavours.add(new DataFlavor(mimeType));

	}

	@Override
	public boolean canImport(TransferSupport support) {

		Transferable t = support.getTransferable();

		for (DataFlavor df : flavours) {
			if (t.isDataFlavorSupported(df)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public boolean importData(TransferSupport support) {

		Transferable t = support.getTransferable();

		DefaultMutableTreeNode parentNode = null;
		Item parentItem = null;

		if (support.isDrop()) {

			support.setShowDropLocation(true);

			DropLocation dropLocation = support.getDropLocation();

			if (dropLocation instanceof JTree.DropLocation) {

				JTree.DropLocation treeDrop = (javax.swing.JTree.DropLocation) dropLocation;

				if (treeDrop.getPath() == null) {
					parentNode = tree.getRootNode();
					parentItem = tree.getRootFolder();
				} else {
					parentNode = (DefaultMutableTreeNode) treeDrop.getPath().getLastPathComponent();
					parentItem = (Item) parentNode.getUserObject();
				}
			}
		} else {

			parentNode = tree.getSelectedNode();
			parentItem = tree.getSelectedItem();

		}

		List<Item> items = new ArrayList<>();

		boolean finish = false;

		Object data = null;

		for (int i = 0; i < support.getDataFlavors().length && !finish; i++) {

			DataFlavor df = support.getDataFlavors()[i];

			if (!support.isDataFlavorSupported(df)) {
				return false;
			}

			try {
				data = t.getTransferData(df);
			} catch (UnsupportedFlavorException | IOException ex) {
				log.error("Error: ", ex);
				return false;
			}

			log.debug(df + " Got data to import: " + data);

			if (df.equals(DataFlavor.javaFileListFlavor)) {

				log.debug("Import files: " + data);
				List<File> files = (List<File>) data;
				for (File file : files) {

					log.info("File: " + file);

					if (FileHelper.isTextFile(file) && JOptionPane.showConfirmDialog(null, "Do you want to import file's content as a text note?", "Import as a note", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
						try {
							Item item = tree.addTextNote(parentItem, file.getName(), FileUtils.readFileToString(file, StandardCharsets.UTF_8));
							items.add(item);
						} catch (IOException e) {
							log.error("Error: ", e);
						}
					} else if (FileHelper.isImageFile(file) && JOptionPane.showConfirmDialog(null, "Do you want to import as an image note?", "Importing image file", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
						try {
							
							Item item = tree.addImage(parentItem, file.getName(), ImageIO.read(file));
							items.add(item);
							
						} catch (IOException e) {
							log.error("Error: ", e);
						}
					} else {
						tree.addFileItem(parentItem, items, file);
					}
				}

			} else if (df.equals(DataFlavor.stringFlavor)) {

				final String string = data.toString().trim();

				log.debug("Import string: " + string);

				List<Item> list = null;
				if (UrlValidator.getInstance().isValid(string)) {
					log.debug("Import as URL item");
					list = tree.addUrlNote(parentItem, string);
				} else {
					log.debug("Import as text note item");

					String title = string.length() > 32 ? string.trim().substring(0, 32) + "..." : string.trim();
					list = Arrays.asList(tree.addTextNote(parentItem, title, string));
				}

				if (list != null) {
					items.addAll(list);
				}

				finish = true;

			} else if (df.equals(DataFlavor.imageFlavor)) {
				log.debug("Import image");

				List<Item> list = tree.addImage(parentItem, (BufferedImage) data);
				items.addAll(list);

				finish = true;
			}

		}

		tree.addItemsToTree(parentNode, items);

		return items.isEmpty() == false;

	}

	@Override
	public int getSourceActions(JComponent c) {
		return COPY;
	}

}
