package com.pim4j.ui.tree;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.DropMode;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.pim4j.Pim4j;
import com.pim4j.domain.FileItem;
import com.pim4j.domain.Item;
import com.pim4j.domain.Item.Type;
import com.pim4j.domain.RootFolder;
import com.pim4j.service.ConfigService;
import com.pim4j.service.IconService;
import com.pim4j.service.ItemService;
import com.pim4j.service.SearchService;
import com.pim4j.ui.tabs.Tabs;

@SuppressWarnings("serial")
@Component
public class Tree extends JPanel implements MouseListener, ActionListener, KeyListener, MouseMotionListener {

	private final Log log = LogFactory.getLog(this.getClass());

	private static final String ADD_FOLDER = "ADD_FOLDER";
	private static final String ADD_TEXT_NOTE = "ADD_TEXT_NOTE";
	private static final String ADD_FILE = "ADD_FILE";
	private static final String ADD_CONTACT = "ADD_CONTACT";
	private static final String DELETE_ITEM = "DELETE_ITEM";
	private static final String RENAME_ITEM = "RENAME_ITEM";

	private JTree tree;

	private ExplorerTreeModel treeModel;

	private RootFolder rootFolder;

	private DefaultMutableTreeNode rootNode;

	@Autowired
	private ItemService itemService;

	@Autowired
	private TreeCellRenderer treeCellRenderer;

	@Autowired
	private SearchService searchService;

	@Autowired
	private IconService icons;

	@Autowired
	private Tabs tabs;

	@Autowired
	private ConfigService config;

	@Autowired
	private TreeTransferHandler treeTransferHandler;

	class ExplorerTreeModel extends DefaultTreeModel {

		public ExplorerTreeModel(TreeNode root) {
			super(root);
		}

		@Override
		public void valueForPathChanged(TreePath path, Object newValue) {

			System.out.println("valueForPathChanged: " + path + ", " + newValue);

			final DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
			final Item item = (Item) node.getUserObject();

			if (item.getName().equals(newValue.toString()) == false) {
				itemService.rename(item, newValue.toString());
			}

			node.setUserObject(item);
		}

	}

	@SuppressWarnings("serial")
	class TreePopupMenu extends JPopupMenu {

		private JMenuItem deleteItem;
		private JMenuItem renameItem;

		public void disableItemsForRoot() {
			deleteItem.setEnabled(false);
			renameItem.setEnabled(false);
		}

		{
			{
				JMenuItem item = new JMenuItem("Add Folder");
				item.setActionCommand(ADD_FOLDER);
				item.addActionListener(Tree.this);
				item.setIcon(icons.getFolderAdd());
				this.add(item);
			}
			{
				JMenuItem item = new JMenuItem("Add Text Note");
				item.setActionCommand(ADD_TEXT_NOTE);
				item.setIcon(icons.getTextNoteAdd());
				item.addActionListener(Tree.this);
				this.add(item);
			}
			{
				JMenuItem item = new JMenuItem("Add File");
				item.setActionCommand(ADD_FILE);
				item.setIcon(icons.getAttachIcon());
				item.addActionListener(Tree.this);
				this.add(item);
			}
			{
				JMenuItem item = new JMenuItem("Add Contact Info");
				item.setActionCommand(ADD_CONTACT);
				item.setIcon(icons.getUserAddIcon());
				item.addActionListener(Tree.this);
				this.add(item);
			}

			this.addSeparator();
			{
				renameItem = new JMenuItem("Rename Item");
				renameItem.setActionCommand(RENAME_ITEM);
				renameItem.addActionListener(Tree.this);
				this.add(renameItem);
			}
			this.addSeparator();
			{
				deleteItem = new JMenuItem("Delete Item");
				deleteItem.setActionCommand(DELETE_ITEM);
				deleteItem.setIcon(icons.getDeleteIcon());
				deleteItem.addActionListener(Tree.this);
				this.add(deleteItem);
			}
		}
	}

	@PostConstruct
	public void init() {

		this.setLayout(new BorderLayout());

		tree = new JTree();
		tree.setEditable(true);
		tree.setDragEnabled(true);
		tree.setDropMode(DropMode.ON_OR_INSERT);
		tree.setTransferHandler(treeTransferHandler);

		TreeCellEditor treeCellEditor = new TreeCellEditor(tree, treeCellRenderer);
		tree.setCellEditor(treeCellEditor);

		rootFolder = new RootFolder();

		rootNode = new DefaultMutableTreeNode(rootFolder);

		treeModel = new ExplorerTreeModel(rootNode);

		// load items into tree
		List<Item> items = itemService.getItems();

		Map<Item, DefaultMutableTreeNode> map = new HashMap<>();

		for (Item item : items) {

			log.debug("Item: " + item);

			DefaultMutableTreeNode node = new DefaultMutableTreeNode(item);

			if (item.getParent().getClass().equals(rootFolder.getClass())) {
				log.debug("Root child: " + item);
				rootNode.add(node);
			} else {
				map.get(item.getParent()).add(node);
			}
			map.put(item, node);

		}

		tree.setCellRenderer(treeCellRenderer);
		tree.setModel(treeModel);
		tree.setShowsRootHandles(false);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

		tree.addKeyListener(this);

		this.add(tree);

		tree.addMouseListener(this);
		tree.addMouseMotionListener(this);

	}

	@Override
	public void mouseClicked(MouseEvent e) {

		if (SwingUtilities.isRightMouseButton(e)) {

			showTreeContextMenu(e.getX(), e.getY());
		}

		if (SwingUtilities.isLeftMouseButton(e) && e.getClickCount() == 2) {
			openSelectedTab();
		}
	}

	private void showTreeContextMenu(int x, int y) {
		int row = tree.getClosestRowForLocation(x, y);
		tree.setSelectionRow(row);

		TreePopupMenu menu = new TreePopupMenu();

		// disable delete on root
		if (((DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent()).getUserObject().getClass().equals(RootFolder.class)) {
			menu.disableItemsForRoot();
		}

		menu.show(tree, x, y);
	}

	private void openSelectedTab() {

		if (tree.getSelectionPath() == null || tree.getSelectionPath().getLastPathComponent() == null) {
			return;
		}

		Item item = (Item) ((DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent()).getUserObject();

		if (item.getType() == Type.TEXT_NOTE) {
			tabs.addTextNote(item.getName(), item.getUuid(), itemService.readTextNote(item.getUuid()));
		} else if (item.getType() == Type.FILE) {
			try {
				File in = new File(config.getDataFolder(), item.getUuid());
				File out = new File(SystemUtils.getJavaIoTmpDir(), item.getName());
				FileUtils.copyFile(in, out);
				log.debug("Created temp file: " + out);
				FileUtils.forceDeleteOnExit(out);
				Desktop.getDesktop().open(out);
			} catch (IOException e) {
				log.error("Error: ", e);
			}
		} else if (item.getType() == Type.IMAGE) {
			try {
				tabs.addImage(item.getName(), item.getUuid(), ImageIO.read(new File(config.getDataFolder(), item.getUuid())));
			} catch (IOException e) {
				log.error("Error reading image or creating image tab: ", e);
			}
		} else if (item.getType() == Type.CONTACT) {
			tabs.addContact(item);
		} else if (item.getType() == Type.URL) {
			openUrl(item);
		}

	}

	private void openUrl(Item item) {
		if (Desktop.isDesktopSupported()) {
			try {
				Desktop.getDesktop().browse(new URI(item.getName()));
			} catch (IOException | URISyntaxException e) {
				JOptionPane.showMessageDialog(null, "Cannot open url: " + item.getName(), "Error opening URL", JOptionPane.ERROR_MESSAGE);
				log.error("Error: ", e);
			}
		}
	}

	public void selectAndOpenByUuid(String uuid) {

		DefaultMutableTreeNode theNode = null;
		for (Enumeration e = rootNode.depthFirstEnumeration(); e.hasMoreElements() && theNode == null;) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.nextElement();
			Item item = (Item) node.getUserObject();
			if (uuid.equals(item.getUuid())) {
				tree.setSelectionPath(new TreePath(node.getPath()));
				openSelectedTab();
				return;
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
		Item parentNote = (Item) parentNode.getUserObject();

		List<Item> newItems = null;

		if (ADD_FOLDER.equals(e.getActionCommand())) {

			newItems = addFolder(parentNote);

		} else if (ADD_TEXT_NOTE.equals(e.getActionCommand())) {

			newItems = addTextNote(parentNote);

		} else if (DELETE_ITEM.equals(e.getActionCommand())) {

			deleteSelectedNode(parentNode, parentNote);

		} else if (RENAME_ITEM.equals(e.getActionCommand())) {

			tree.startEditingAtPath(tree.getSelectionPath());

		} else if (ADD_FILE.equals(e.getActionCommand())) {

			newItems = addFile(parentNote);

		} else if (ADD_CONTACT.equals(e.getActionCommand())) {

			newItems = addContactInfo(parentNote);

		}

		addItemsToTree(parentNode, newItems);
	}

	private List<Item> addContactInfo(Item parentNote) {

		Item note = new Item();
		note.setType(Type.CONTACT);
		note.setName("New Contact");
		note.setUuid(UUID.randomUUID().toString());

		note.setParent(parentNote);

		itemService.addItem(note);

		tabs.addContact(note);

		return Arrays.asList(note);

	}

	public void addItemsToTree(DefaultMutableTreeNode parentTreeNode, List<Item> childItems) {
		if (childItems != null && childItems.isEmpty() == false) {

			for (Item child : childItems) {
				DefaultMutableTreeNode newChildNode = new DefaultMutableTreeNode(child);
				parentTreeNode.add(newChildNode);

				// treeModel.reload(parentFolder);

				tree.setSelectionPath(new TreePath(newChildNode.getPath()));
			}

			treeModel.nodeStructureChanged(parentTreeNode);

		}
	}

	private void deleteSelectedNode(DefaultMutableTreeNode node, Item item) {
		int showConfirmDialog = JOptionPane.showConfirmDialog(null, "Do you want to delete this item?", "Are you sure?", JOptionPane.YES_NO_CANCEL_OPTION);
		if (showConfirmDialog == JOptionPane.YES_OPTION) {

			itemService.delete(item);
			treeModel.removeNodeFromParent(node);

			tree.setSelectionPath(new TreePath(node.getPath()));
		}
	}

	private List<Item> addFile(Item parent) {

		final JFileChooser fc = new JFileChooser();
		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fc.setDialogTitle("Select files...");
		fc.setFileHidingEnabled(false);
		fc.setMultiSelectionEnabled(true);
		fc.setSize(1024, 768);

		int returnVal = fc.showOpenDialog(Pim4j.getMainFrame());

		List<Item> items = new ArrayList<>();

		if (JFileChooser.APPROVE_OPTION == returnVal && fc.getSelectedFile() != null) {

			final File[] files = fc.getSelectedFiles();

			log.info("Selected: " + Arrays.toString(files));

			for (File file : files) {

				addFileItem(parent, items, file);

			}

		}

		return items;

	}

	public void addFileItem(Item parent, List<Item> items, File file) {

		log.info("File: " + file);

		FileItem fileItem = new FileItem();
		try {
			fileItem.setContentType(Files.probeContentType(file.toPath()));
		} catch (Exception e) {
			log.error("Error: ", e);
		}
		fileItem.setType(Type.FILE);
		fileItem.setName(file.getName());
		fileItem.setUuid(UUID.randomUUID().toString());

		fileItem.setParent(parent);

		log.info("Created item: " + ToStringBuilder.reflectionToString(fileItem));

		// copy file to data folder
		itemService.copyFile(fileItem, file);

		// copy icon to icons folder
		itemService.copyIcon(file, fileItem.getContentType());

		itemService.addItem(fileItem);

		items.add(fileItem);
	}

	public List<Item> addTextNote(Item parent) {
		return addTextNote(parent, StringUtils.EMPTY);
	}

	public List<Item> addTextNote(Item parent, String text) {

		String name = JOptionPane.showInputDialog(Pim4j.getMainFrame(), "Enter text note name", "New Text Note", 3);

		if (StringUtils.isBlank(name)) {
			return null;
		}

		Item note = addTextNote(parent, name, text);

		return Arrays.asList(note);

	}

	public Item addTextNote(Item parent, String name, String text) {

		Item note = new Item();
		note.setType(Type.TEXT_NOTE);
		note.setName(name.trim());
		note.setUuid(UUID.randomUUID().toString());

		note.setParent(parent);

		itemService.addItem(note);

		tabs.addTextNote(name, note.getUuid(), text);
		return note;
	}

	public List<Item> addFolder(Item parent) {

		String name = JOptionPane.showInputDialog(Pim4j.getMainFrame(), "Enter folder name", "New Folder", 3);

		if (StringUtils.isBlank(name)) {
			return null;
		}

		Item folder = new Item();
		folder.setType(Type.FOLDER);
		folder.setName(name.trim());
		folder.setUuid(UUID.randomUUID().toString());

		folder.setParent(parent);

		itemService.addItem(folder);

		return Arrays.asList(folder);

	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyPressed(KeyEvent e) {

		if (tree.isSelectionEmpty()) {
			log.warn("No selection");
			return;
		}

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();

		if (node.getUserObject() instanceof Item) {

			Item item = (Item) node.getUserObject();

			if (e.getKeyCode() == KeyEvent.VK_ENTER) {

				openSelectedTab();

			} else if (e.getKeyCode() == KeyEvent.VK_DELETE) {

				if (node.isRoot() == false) {
					deleteSelectedNode(node, item);
				}

			}
		} else {
			// renaming
			"".toCharArray();
		}

	}

	public RootFolder getRootFolder() {
		return rootFolder;
	}

	public DefaultMutableTreeNode getRootNode() {
		return rootNode;
	}

	public DefaultMutableTreeNode getSelectedNode() {
		if (tree.getSelectionPath() == null) {
			return rootNode;
		}

		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();
		return node;
	}

	public Item getSelectedItem() {
		if (tree.getSelectionPath() == null) {
			return rootFolder;
		}
		DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getSelectionPath().getLastPathComponent();
		return (Item) node.getUserObject();
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseMoved(MouseEvent e) {

		TreePath tp = ((JTree) e.getSource()).getPathForLocation(e.getX(), e.getY());

		if (tp != null) {
			((JTree) e.getSource()).setCursor(new Cursor(Cursor.HAND_CURSOR));
		} else {
			((JTree) e.getSource()).setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
	}

	public List<Item> addImage(Item parentItem, BufferedImage data) {

		String name = JOptionPane.showInputDialog(Pim4j.getMainFrame(), "Enter image name", "New Image", 3);

		if (StringUtils.isBlank(name)) {
			return null;
		}

		Item note = addImage(parentItem, name, data);

		return Arrays.asList(note);

	}

	public Item addImage(Item parent, String name, BufferedImage data) {

		Item item = new Item();
		item.setType(Type.IMAGE);
		item.setName(name.trim());
		item.setUuid(UUID.randomUUID().toString());

		item.setParent(parent);

		itemService.addItem(item);

		try {
			File file = new File(config.getDataFolder(), item.getUuid());
			log.debug("Saving image: " + file);
			ImageIO.write(data, "png", file);
		} catch (IOException e) {
			log.debug("Error saving image: ", e);
		}

		tabs.addImage(name, item.getUuid(), data);

		return item;
	}

	public List<Item> addUrlNote(Item parentItem, String url) {

		Item item = new Item();
		item.setType(Type.URL);
		item.setName(url);
		item.setUuid(UUID.randomUUID().toString());

		item.setParent(parentItem);

		itemService.addItem(item);

		openUrl(item);

		return Arrays.asList(item);
	}

}
