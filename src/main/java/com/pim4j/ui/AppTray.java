package com.pim4j.ui;

import java.awt.SystemTray;
import java.awt.TrayIcon;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.pim4j.service.IconService;

@Component
public class AppTray {

	private static final Log log = LogFactory.getLog(AppTray.class);

	private TrayIcon trayIcon;

	@PostConstruct
	public void init() {

		if (!SystemTray.isSupported()) {
			log.error("SystemTray is not supported");
			return;
		}
		trayIcon = new TrayIcon(IconService.load("app").getImage());
		trayIcon.setToolTip("Pim4j");
		final SystemTray tray = SystemTray.getSystemTray();

		try {
			tray.add(trayIcon);
		} catch (Exception e) {
			log.error("TrayIcon could not be added.");
		}

	}

	public void showInfoMessage(String title, String msg) {
		trayIcon.displayMessage(title, msg, TrayIcon.MessageType.INFO);
	}

}
