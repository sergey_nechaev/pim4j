package com.pim4j.domain;

public class FileItem extends Item {

	private String contentType;

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType == null ? null : contentType.replace("/", "-");
	}

}
