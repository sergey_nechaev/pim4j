package com.pim4j;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.pim4j.ui.AppTray;
import com.pim4j.ui.tree.Tree;
import com.pim4j.ui.window.MainFrame;

public class Pim4j {

	private static final Log log = LogFactory.getLog(Pim4j.class);

	private static ClassPathXmlApplicationContext ctx;

	public static void main(String[] args) {

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception e) {
			log.error(e);
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception ex) {
				log.error(ex);
			}
		}

		ctx = new ClassPathXmlApplicationContext("classpath:/com/pim4j/spring-context.xml");
		ctx.registerShutdownHook();

		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				getMainFrame().run();
			}
		});

	}

	public static MainFrame getMainFrame() {
		return ctx.getBean(MainFrame.class);
	}

	public static void exit() {

		if (ctx != null) {
			ctx.stop();
			ctx.destroy();
		}

		System.exit(0);
	}

	public static Tree getTree() {
		return ctx.getBean(Tree.class);
	}

	public static AppTray getSystemTray() {
		return ctx.getBean(AppTray.class);
	}

}
