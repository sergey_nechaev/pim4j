package com.pim4j.service;

import java.net.InetAddress;
import java.net.ServerSocket;

import javax.annotation.PostConstruct;
import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.pim4j.Pim4j;

@Component
public class SingleInstanceChecker {

	private Log log = LogFactory.getLog(this.getClass());

	@PostConstruct
	public void start() {

		try {
			ServerSocket socket = new ServerSocket(9999, 10, InetAddress.getLocalHost());

		} catch (java.net.BindException b) {

			JOptionPane.showMessageDialog(null, "There is an instance of Pim4j already running");

			Pim4j.exit();

		} catch (Exception e) {
			System.out.println(e.toString());
		}

	}

}
