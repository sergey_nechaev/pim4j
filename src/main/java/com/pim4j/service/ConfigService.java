package com.pim4j.service;

import java.io.File;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

@Service
public class ConfigService {

	private Log log = LogFactory.getLog(this.getClass());

	private final File CONFIG_FILE = new File(getRootFolder() + "config");

	@PostConstruct
	public void init() {

		if (CONFIG_FILE.exists() == false) {
			log.info("Config file doesn't exist: " + CONFIG_FILE);
			// JOptionPane.showInputDialog(Pim4j.getMainFrame(),"Enter your
			// message","Messages",2);
		}

	}

	public String getRootFolder() {
		return SystemUtils.USER_HOME + File.separator + "pim4j" + File.separator;
	}

	public String getSearchFolder() {
		return getRootFolder() + "search" + File.separator;
	}

	public String getDataFolder() {
		return getRootFolder() + "data" + File.separator;
	}

	public String getItemsFolder() {
		return getRootFolder() + "items" + File.separator;
	}

	public String getIconsFolder() {

		final String folder = getRootFolder() + "icons" + File.separator;

		final File folderFile = new File(folder);

		if (false == folderFile.exists()) {
			try {
				FileUtils.forceMkdir(folderFile);
			} catch (IOException e) {
				log.error("Error: ", e);
			}
		}

		return folder;
	}

	public String getLogFolder() {
		return SystemUtils.USER_HOME + File.separator + "pim4j" + File.separator + "logs" + File.separator;
	}

}
