package com.pim4j.service;

import java.util.ArrayList;
import java.util.List;

public class SearchResults {

	public static class SearchResult {
		public String uuid;
		public String snippet;
		public String title;
	}
	
	private String query;

	private List<SearchResult> results = new ArrayList<>();

	private int total;
	
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public List<SearchResult> getResults() {
		return results;
	}

	public void setResults(List<SearchResult> results) {
		this.results = results;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

}
