package com.pim4j.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;
import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexNotFoundException;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.flexible.standard.QueryParserUtil;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.search.highlight.TextFragment;
import org.apache.lucene.search.highlight.TokenSources;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pim4j.service.SearchResults.SearchResult;

@Service
public class SearchService {

	private static final String FIELD_UUID = "uuid";

	private static final String FIELD_TEXT = "text";

	private final Log log = LogFactory.getLog(this.getClass());

	@Autowired
	private ConfigService config;

	@Autowired
	private ItemService itemService;

	@PostConstruct
	public void init() {

		String docsPath = config.getSearchFolder();

		log.debug("Search folder: " + docsPath);

		final Path indexDir = Paths.get(docsPath);

		if (!Files.exists(indexDir)) {
			try {
				Files.createDirectories(indexDir);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Document directory '" + indexDir.toAbsolutePath() + "' cannot be created", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}

		if (!Files.isReadable(indexDir) || !Files.isWritable(indexDir)) {
			JOptionPane.showMessageDialog(null, "Document directory '" + indexDir.toAbsolutePath() + "' does not exist or is not readable/writable, please check the path", "Error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}

	}

	public void index(String uuid, String title, String text) {

		String indexPath = config.getSearchFolder();

		try {

			Directory dir = FSDirectory.open(Paths.get(indexPath));
			Analyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			IndexWriter writer = new IndexWriter(dir, iwc);

			Document doc = new Document();

			StringField uuidField = new StringField(FIELD_UUID, uuid, Field.Store.YES);
			doc.add(uuidField);

			TextField textField = new TextField(FIELD_TEXT, text, Field.Store.YES);
			doc.add(textField);

			writer.updateDocument(new Term(FIELD_UUID, uuid), doc);

			writer.close();

		} catch (IOException e) {
			log.error(e);
			JOptionPane.showMessageDialog(null, "Search functionality error", "Error", JOptionPane.ERROR_MESSAGE);
		}

	}

	public SearchResults search(String line) {

		SearchResults result = new SearchResults();
		result.setQuery(line);

		log.debug("Search for: " + line);

		if (StringUtils.isBlank(line)) {
			return result;
		}

		line = QueryParserUtil.escape(line);

		String indexPath = config.getSearchFolder();

		try {

			IndexReader reader = null;
			try {
				reader = DirectoryReader.open(FSDirectory.open(Paths.get(indexPath)));
			} catch (IndexNotFoundException e) {
				log.warn("Search index doesn't exist yet: " + indexPath);
				return result;
			}

			IndexSearcher searcher = new IndexSearcher(reader);
			Analyzer analyzer = new StandardAnalyzer();
			QueryParser parser = new QueryParser(FIELD_TEXT, analyzer);
			Query query = parser.parse(line);

			TopDocs results = null;
			try {
				results = searcher.search(query, reader.maxDoc());
			} catch (Exception e) {
				log.error(e);
				return result;
			}

			SimpleHTMLFormatter htmlFormatter = new SimpleHTMLFormatter();
			Highlighter highlighter = new Highlighter(htmlFormatter, new QueryScorer(query));

			ScoreDoc[] hits = results.scoreDocs;
			int numTotalHits = results.totalHits;
			log.info(numTotalHits + " total matching documents");

			result.setTotal(numTotalHits);

			for (int i = 0; i < hits.length; i++) {
				Document doc = searcher.doc(hits[i].doc);

				int id = hits[i].doc;

				SearchResult sr = new SearchResult();
				sr.uuid = doc.get(FIELD_UUID);
				sr.title = itemService.getItem(sr.uuid).getName();

				String text = doc.get(FIELD_TEXT);
				StringBuilder sb = new StringBuilder("...");

				TokenStream tokenStream = TokenSources.getAnyTokenStream(reader, id, FIELD_TEXT, analyzer);
				TextFragment[] frag = highlighter.getBestTextFragments(tokenStream, text, false, 4);
				for (int j = 0; j < frag.length; j++) {
					if ((frag[j] != null) && (frag[j].getScore() > 0)) {
						sb.append(frag[j].toString());
						sb.append("...");
					}
				}
				sr.snippet = sb.toString();

				result.getResults().add(sr);
			}

			reader.close();

		} catch (Exception e) {
			log.error("Error: ", e);
			JOptionPane.showMessageDialog(null, "Search functionality error", "Error", JOptionPane.ERROR_MESSAGE);
		}

		return result;
	}

	public void delete(String uuid) {

		String indexPath = config.getSearchFolder();

		try {

			Directory dir = FSDirectory.open(Paths.get(indexPath));
			Analyzer analyzer = new StandardAnalyzer();
			IndexWriterConfig iwc = new IndexWriterConfig(analyzer);
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			IndexWriter writer = new IndexWriter(dir, iwc);

			writer.deleteDocuments(new Term(FIELD_UUID, uuid));

			writer.close();

		} catch (IOException e) {
			log.error(e);
			JOptionPane.showMessageDialog(null, "Delete search functionality error", "Error", JOptionPane.ERROR_MESSAGE);
		}

	}

}
