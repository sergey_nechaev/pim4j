package com.pim4j.service;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pim4j.common.ZIPHelper;
import com.pim4j.domain.FileItem;
import com.pim4j.domain.Item;
import com.pim4j.ui.tabs.Tabs;
import com.thoughtworks.xstream.XStream;

@Service
public class ItemService {

	@Autowired
	private ConfigService configService;

	private Log log = LogFactory.getLog(this.getClass());

	private ArrayList<Item> items = new ArrayList<>();

	private XStream xstream = new XStream();

	@Autowired
	private SearchService searchService;

	@Autowired
	private Tabs tabs;

	@Autowired
	private IconService icons;

	@PostConstruct
	public void init() {

		File file = getItemsFile();

		if (file.exists()) {
			try {

				log.debug("Reading items: " + file);
				byte[] data = FileUtils.readFileToByteArray(file);
				String xml = ZIPHelper.decompress(data);
				items = (ArrayList) xstream.fromXML(xml);
			} catch (IOException e) {
				log.error("Error: ", e);
				JOptionPane.showMessageDialog(null, "Can not read items: " + file, "Error", JOptionPane.ERROR_MESSAGE);
			}
		}

	}

	public List<Item> getChildrenRecursively(Item parent) {

		List<Item> list = new ArrayList<>();

		getChildrenRecursively(list, parent);

		return list;

	}
	
	public void rename(Item item, String name) {
		item.setName(name);
		this.saveItems();
	}

	public List<Item> getChildrenRecursively(List<Item> list, Item parent) {

		list.add(parent);

		for (Item item : this.items) {
			if (item.getParent().equals(parent)) {
				getChildrenRecursively(list, item);
			}
		}

		return list;
	}

	public List<Item> getItems() {
		return Collections.unmodifiableList(items);
	}

	public Item getItem(String uuid) {
		for (Item item : items) {
			if (item.getUuid().equals(uuid)) {
				return item;
			}
		}
		return null;
	}

	public void addItem(Item folder) {

		log.debug("Add item: " + folder);

		items.add(folder);

		saveItems();

	}

	public String readTextNote(String uuid) {

		File file = new File(configService.getDataFolder(), uuid);
		String content = "";
		try {
			byte[] data = FileUtils.readFileToByteArray(file);
			content = ZIPHelper.decompress(data);
			log.info("Read text file: " + file);
		} catch (IOException e) {
			log.error("Can not read text file: " + file);
			JOptionPane.showMessageDialog(null, "Can not read text file: " + file, "Error", JOptionPane.ERROR_MESSAGE);

		}
		return content;

	}

	private void saveItems() {

		String xml = xstream.toXML(items);
		byte[] zip = ZIPHelper.compress(xml);

		File file = getItemsFile();

		if (file.exists()) {
			file.delete();
		}

		try {
			FileUtils.writeByteArrayToFile(file, zip);
			log.info("Saved items file: " + file);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Can not save items: " + file, "Error", JOptionPane.ERROR_MESSAGE);
		}

	}

	private File getItemsFile() {
		File file = new File(configService.getItemsFolder(), "items");
		return file;
	}

	public void saveContent(String uuid, String content) {

		File file = new File(configService.getDataFolder(), uuid);

		if (file.exists()) {
			file.delete();
		}

		try {
			byte[] zip = ZIPHelper.compress(content);

			FileUtils.writeByteArrayToFile(file, zip);
			log.info("Saved text file: " + file);
		} catch (IOException e) {
			log.error("Can not save text file: " + file);
			JOptionPane.showMessageDialog(null, "Can not save text file: " + file, "Error", JOptionPane.ERROR_MESSAGE);

		}

	}

	public void delete(Item parent) {

		System.out.println("Delete " + parent);

		// find all children
		List<Item> items_to_delete = this.getChildrenRecursively(parent);

		System.out.println("Deleting: " + items_to_delete);

		// reverse to start with the leafs
		Collections.reverse(items_to_delete);

		for (Item item : items_to_delete) {

			if (item.getType().equals(Item.Type.FOLDER)) {
				// no physical data to delete
			} else if (item.getType().equals(Item.Type.TEXT_NOTE)) {
				deleteTextNote(item);
			} else if (item.getType().equals(Item.Type.FILE)) {
				deleteFile(item);
			}

			// close tab
			tabs.closeTabByUuid(item.getUuid());

			// physical remove from item storage
			this.items.remove(item);

		}

		saveItems();

	}

	private void deleteFile(Item item) {

		File file = new File(configService.getDataFolder(), item.getUuid());

		if (file.exists()) {
			file.delete();
		}

		searchService.delete(item.getUuid());

	}

	private void deleteTextNote(Item item) {

		File file = new File(configService.getDataFolder(), item.getUuid());

		if (file.exists()) {
			file.delete();
		}

		searchService.delete(item.getUuid());

	}

	public void copyFile(FileItem fileItem, File file) {
		File outputFile = new File(configService.getDataFolder(), fileItem.getUuid());
		try {
			FileUtils.copyFile(file, outputFile);
			log.debug("Copied: " + file + " to " + outputFile);
		} catch (Exception e) {
			log.error("Error: ", e);
		}
	}
	
	public void copyIcon(File file, String contentType) {
		if (StringUtils.isBlank(contentType)) {
			return;
		}
		Icon systemIcon = icons.getSystemIcon(file);
		BufferedImage bim = new BufferedImage(systemIcon.getIconWidth(), systemIcon.getIconHeight(), BufferedImage.TYPE_4BYTE_ABGR);
		Graphics g = bim.getGraphics();
		systemIcon.paintIcon(null, g, 0, 0);
		g.dispose();
		try {
			ImageIO.write(bim, "png", new File(configService.getIconsFolder(), contentType));
		} catch (IOException e) {
			log.error("Error: ", e);
		}
	}

}
