package com.pim4j.service;

import java.io.File;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.filechooser.FileSystemView;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IconService {

	private final Log log = LogFactory.getLog(IconService.class);

	private FileSystemView fsv = FileSystemView.getFileSystemView();

	@Autowired
	private ConfigService config;

	private final ImageIcon APP = getIcon("app");

	private final ImageIcon ROOT = getIcon("folder");
	private final ImageIcon FOLDER = getIcon("folder");
	private final ImageIcon FOLDER_ADD = getIcon("folder_add");
	private final ImageIcon TEXT_NOTE = getIcon("page_green");
	private final ImageIcon TEXT_NOTE_ADD = getIcon("page_add");
	private final ImageIcon MAGNIFIER = getIcon("magnifier");
	private final ImageIcon DELETE = getIcon("delete");
	private final ImageIcon ATTACH = getIcon("attach");
	private final ImageIcon HELP = getIcon("help");
	private final ImageIcon DATABASE = getIcon("database");
	private final ImageIcon SCRIPT = getIcon("script");
	private final ImageIcon IMAGE = getIcon("picture");
	private final ImageIcon BULLET_GO = getIcon("bullet_go");

	private final ImageIcon USER = getIcon("user");
	private final ImageIcon USER_ADD = getIcon("user_add");
	private final ImageIcon LINK = getIcon("link");

	private final Map<String, Icon> SYSTEM_ICONS = new HashMap<>();

	public static ImageIcon load(String name) {
		return new ImageIcon(IconService.class.getResource("/icons/" + name + ".png"));
	} 
	
	private ImageIcon getIcon(String name) {
		return new ImageIcon(this.getClass().getResource("/icons/" + name + ".png"));
	}

	public ImageIcon getTextNoteAdd() {
		return TEXT_NOTE_ADD;
	}

	public ImageIcon getFolderAdd() {
		return FOLDER_ADD;
	}

	public ImageIcon getMagnifier() {
		return MAGNIFIER;
	}

	public ImageIcon getAppIcon() {
		return APP;
	}

	public ImageIcon getRootIcon() {
		return ROOT;
	}

	public ImageIcon getFolderIcon() {
		return FOLDER;
	}

	public ImageIcon getTextNoteIcon() {
		return TEXT_NOTE;
	}

	public Icon getDeleteIcon() {
		return DELETE;
	}

	public Icon getAttachIcon() {
		return ATTACH;
	}

	public Icon getHelpIcon() {
		return HELP;
	}

	public Icon getSystemIconByContentType(String contentType) {
		if (StringUtils.isBlank(contentType)) {
			return getAttachIcon();
		}
		Icon icon = SYSTEM_ICONS.get(contentType);
		if (icon == null) {

			try {
				icon = new ImageIcon(new File(config.getIconsFolder(), contentType).toURL());
				SYSTEM_ICONS.put(contentType, icon);
			} catch (MalformedURLException e) {
				log.error("Error: ", e);
			}

			return ATTACH;
		}
		return icon;
	}

	public Icon getSystemIcon(File file) {
		return fsv.getSystemIcon(file);

	}

	public ImageIcon getDatabaseIcon() {
		return DATABASE;
	}

	public Icon getScriptIcon() {
		return SCRIPT;
	}

	public Icon getImageIcon() {
		return IMAGE;
	}

	public Icon getUserIcon() {
		return USER;
	}

	public Icon getUserAddIcon() {
		return USER_ADD;
	}

	public Icon getBulletGoIcon() {
		return BULLET_GO;
	}

	public Icon getLinkIcon() {
		return LINK;
	}

}
